import java.util.ArrayList;

class MyException extends Exception{
	private static final long serialVersionUID = 1L;
}

public class TestHuman {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//int i;
		Human human=new Human("�","����");
		human.showInfo();
		
		Student student = new Student("�","����","�����������");
		student.showInfo();

		Worker worker = new Worker("�","����","�����","���-�����");
		worker.showInfo();
		
		//����� ��������� ��� ���������� ������ ������, � ��������� ���� ����� ��������� ��������! 
		ArrayList<String> al_str = new ArrayList<>();
		al_str.add("������1");
		al_str.add("������ ����� 2");
		al_str.add("������ � ���");
		
		//������� �� ������� ��������� ���� for ������� �� �������
		for(String s:al_str) System.out.println(s);
		try{
			throw new MyException();
		}catch(Exception e){
			System.out.println("���������� MyException()");
		};
		
	}

}
